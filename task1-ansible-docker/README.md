![SlovenskoIT](https://gitlab.com/devops301/hireme/-/raw/master/SlovenskoIT_logo.svg)

## Automated deployment of static web page in docker container

Use Ansible to build and deploy docker container on Linux server (choose distro based on your preferences)

What steps should Ansible playbook/roles cover:

1. Create user deploy
2. Install docker
3. Create `Dockerfile` which you use to build docker image with web server (again it is up to you to choose aby web server you like) and with any static web page or json file or any static data you would like to serve.
4. Build docker container
5. Run docker container


### Requirements
* Please use Ansible roles (https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
* Allow modification of name of image, container and port where web server is listening via Ansible vars
* Make playbook/roles as much indempotent as possible (repeatable execution of playbook changes only tasks which need to be changed)

### How to Proceed

Prepare playbooks/scripts locally, create Git Repo on public service and commit the code there with short description in README.md how to use the playbooks. Please send the link to HR representative, you have been in contact with, or directly to devops@slovenskoit.sk. We will get in touch with you afterwards.

