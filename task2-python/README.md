![SlovenskoIT](https://gitlab.com/devops301/hireme/-/raw/master/SlovenskoIT_logo.svg)

## Script which sends email notification when DigitalOcean is down

Prepare python script which sends an email to alert email address when our cloud provider (DigitalOcean) has problems.

### Specification

* Script compatible with python3
* If possible, use only stdlib functions and do not use external libraries (no pip install)
* Email address where to send alert needs to be defined by environment variable `SKIT_ALERT_EMAIL`
* SMTP settings should also be configurable via environment variables
* Script will be executed in Cron or similar job scheduler every hour

### DO API

You can find documentation for DO Status API at https://status.digitalocean.com/api/v2

### How to Proceed

Prepare scripts locally, create Git Repo on public service and commit the code there with short description in README.md how to use the script. Please send the link to HR representative, you have been in contact with, or directly to devops@slovenskoit.sk. We will get in touch with you afterwards.

### Bonus points

* Logging via python logging module

